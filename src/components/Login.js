import Welcome from "./Welcome";
// import App from "../App";
export default function Login({ onInputUser, onInputPass, onSign, userValue, passValue, sucValue }) {

    // NOTE: ถ้ามีค่า
    // if(success) Welcome...
    // const onSignIn = () => {
    //     if (sucValue === false) {
    //         console.log("xxxx" + sucValue)
    //     } else if (sucValue === true) {
    //         console.log("++++" + sucValue)
    //     }

    // }
    if (sucValue === true) {
        return <Welcome></Welcome>
    }



    return (
        <div>
            {/* ⚠️ ควรมี name, id */}
            <input name="Username" id="username" onChange={onInputUser} value={userValue} />

            {/* ⚠️ ควรมี name, id */}
            <input name="Password" id="password" onChange={onInputPass} value={passValue} />


            <button onClick={onSign}>Sign in</button>
        </div>
    );
}