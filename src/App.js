import { useState } from "react";
import Login from "./components/Login";



export default function App() {
    const [userList, setUserList] = useState("");
    const [passList, setPassList] = useState("");
    const [success, setSuccess] = useState(false);


    const onInputUser = (event) => {
        setUserList(event.target.value); // ❌ string -> array
    }


    const onInputPass = (event) => {
        setPassList(event.target.value); // ❌ string -> array
    }


    // NOTE: ตัวแทนของการคลิกปุ่ม
    const onSign = () => {
        setSuccess(userList === "ABC" && passList === "1234") // ❌ boolean -> array
    }

    return (
        <div>
            <Login onInputUser={onInputUser} userValue={userList} onInputPass={onInputPass} passValue={passList} onSign={onSign} sucValue={success}></Login>
        </div>
    );
}
